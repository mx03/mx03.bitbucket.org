function PFileReader(file)
{
	var reader = new FileReader();
	return new Promise((resolve, reject) => {
		reader.onerror = reject;
		reader.onloadend = resolve;
		reader.readAsDataURL(file);
	});
}