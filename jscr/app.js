navigator.serviceWorker.register('sw.js').then(function(reg) {
	// Registrierung erfolgreich
	console.log('Registrierung erfolgreich. Scope ist ' + reg.scope);
	}).catch(function(error) {
	// Registrierung fehlgeschlagen
	console.log('Registrierung fehlgeschlagen mit ' + error);
});

function randomInt(max) {
	return Math.floor(Math.random() * Math.floor(max));
}
document.addEventListener("DOMContentLoaded", function() {
	var img_height = 720;
	var img_width = 1080;
	var filename = "img";

	let deferredPrompt;
	var exifObj;

	var renderer_el = document.querySelector("#renderer");
	var label_text = document.querySelector("input[name=label]");
	var label_color = document.querySelector("input[name=labelcolor]");
	var btninstall = document.querySelector("#btninstall");

	window.addEventListener('beforeinstallprompt', (e) => {
		// Prevent Chrome 67 and earlier from automatically showing the prompt
		e.preventDefault();
		// Stash the event so it can be triggered later.
		deferredPrompt = e;
		btninstall.style.display = 'inline-block';
	});

	var thecropper = new Cropper(renderer_el, {
		aspectRatio: img_width / img_height,
		autoCropArea: 1
	});

	label_text.value = localStorage.getItem('label_text');
	label_color.value = localStorage.getItem('label_color');

	label_text.addEventListener('change', function(){
		localStorage.setItem('label_text', this.value);
	});
	label_color.addEventListener('change', function(){
		localStorage.setItem('label_color', this.value);
	});
	
	document.querySelector("input[name=img]").addEventListener('change', async function() {
		filename = this.files[0].name;
		var img_url = URL.createObjectURL(this.files[0]);
		try
		{
			var filee  = await PFileReader(this.files[0]);
		}
		catch(e)
		{
			console.log(e);
		}
		if(this.files[0].type === 'image/jpeg')
		{
			exifObj = piexif.load(filee.target.result);
			exifObj.thumbnail = null;
		}

		renderer_el.src = img_url;
		thecropper.clear()
		thecropper.replace(img_url);
		
	});

	document.querySelector("#btnopen").addEventListener('click', function() {
		document.querySelector("input[name=img]").click();
	});
	document.querySelector("#btnrotate").addEventListener('click', function() {
		if(img_height === 720)
		{
			img_height = 1080;
			img_width = 720;
		}
		else
		{
			img_height = 720;
			img_width = 1080;
		}
		thecropper.setAspectRatio(img_width / img_height);
	});

	document.querySelector("#btnsave").addEventListener('click', function() {
		var exifbytes = null;
		if(exifObj)
		{
			exifObj.thumbnail = null;
			var exifbytes = piexif.dump(exifObj);
		}

		var canvas = thecropper.getCroppedCanvas({
			width: img_width,
			height: img_height
		});

		var ctx = canvas.getContext('2d');
		ctx.fillStyle = label_color.value;
		ctx.font = "16px Courier New";
		var texty = img_height - 5;
		ctx.fillText(label_text.value, 5, texty);

		canvas.toBlob(async function (blob) {
			try
			{
				if(exifbytes)
				{
					var filee  = await PFileReader(blob);
					var inserted = piexif.insert(exifbytes, filee.target.result);
					var res = await fetch(inserted);
					blob = await res.blob();
				}
				saveAs(blob, filename);
			}
			catch(e)
			{
				console.log(e);
			}
		}, "image/jpeg");
	});
});